
<!-- README.md is generated from README.Rmd. Please edit that file -->

# maniphyt

<!-- badges: start -->
<!-- badges: end -->

Utility and auxiliary functions to manipulate phytosociological tables
and related data (relevés, headers etc.).

## Installation

You can install the package from GitLab.

``` r
devtools::install_git("https://gitlab.com/point-veg/maniphyt")
```
