# clean_empty_rows.R
#'
#' @title Delete empty rows from a relevé table
#'
#' @description This function deletes the empty rows from a relevé table.
#'
#' @param x `matrix` A relevé table.
#' @param absence.values `character` A vector, with values representing absences. Defaults to c(".","0").
#' @param preserve.cols `numeric` A `vector` containing the indices of the columns that should not be considered while cleaning (e.g., that don't correspond to relevés, but to taxa names).
#' @param preserve.rows `numeric` A `vector` containing the indices of the rows that should not be considered while cleaning (e.g., that don't correspond to taxa, but to header lines).
#'
#' @details This function deletes the empty rows from a relevé table. Row names from deleted rows will be lost.
#' Column and row order might be changed, as preserved columns (`preserve.cols`) are placed on the left part of the output,
#' and preserved rows (`preserve.rows`) are placed in the top part of the output.
#'
#' @return A `matrix` as (`x`), possibly without empty rows. Column and row order might be changed. See Details.
#'
#' @author Tiago Monteiro-Henriques. E-mail: \email{tmh.dev@@icloud.com}.
#'
#' @export
#'
clean_empty_rows <- function (x, absence.values=c(".","0"), preserve.cols=NULL, preserve.rows=NULL) {
  stopifnot(is.matrix(x))
  not.absent <- function (y) {
    return(!apply(y, 2, match, absence.values, 0))
  }
  if (is.null(preserve.cols) & is.null(preserve.rows)) {
    not.abs <- not.absent(x)
    dim(not.abs) <- c(nrow(x), ncol(x))
    for.rows <- apply(not.abs, 1, any)
    return(x[for.rows, , drop=F])
  }
  if (!is.null(preserve.cols) & !is.null(preserve.rows)) {
    x.temp <- x[-preserve.rows, -preserve.cols, drop = F]
    not.abs <- not.absent(x.temp)
    dim(not.abs) <- c(nrow(x.temp), ncol(x.temp))
    for.rows <- apply(not.abs, 1, any)
    return(rbind(
      x[preserve.rows, c(preserve.cols, (1:ncol(x))[-preserve.cols]), drop = F],
      x[-preserve.rows, c(preserve.cols, (1:ncol(x))[-preserve.cols]), drop = F][for.rows, , drop=F]
    ))
  }
  if (!is.null(preserve.cols)) {
    x.temp <- x[, -preserve.cols, drop=F]
    not.abs <- not.absent(x.temp)
    dim(not.abs) <- c(nrow(x.temp), ncol(x.temp))
    for.rows <- apply(not.abs, 1, any)
    return(x[for.rows, c(preserve.cols, (1:ncol(x))[-preserve.cols]), drop = F])
  }
  if (!is.null(preserve.rows)) {
    x.temp <- x[-preserve.rows, , drop=F]
    not.abs <- not.absent(x.temp)
    dim(not.abs) <- c(nrow(x.temp), ncol(x.temp))
    for.rows <- apply(not.abs, 1, any)
    return(rbind(
      x[preserve.rows, , drop = F],
      x[-preserve.rows, , drop = F][for.rows, , drop=F]
    ))
  }
}

